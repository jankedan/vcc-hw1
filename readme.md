### HW1 - 15b

### Dashboard
![](./dashboard.png)

### Instances
![](./instances.png)

### Network detail
![](./network-detail.png)

### Demo1 detail
![](./demo1-detail.png)

### Demo1 ping
![](./demo1-ping.png)

### Demo2 detail
![](./demo2-detail.png)

### Demo2 ping
![](./demo2-ping.png)